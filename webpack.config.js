const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var autoprefixer = require('autoprefixer');
var precss = require('precss');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './code/src/index.html',
  filename: 'index.html',
  inject: 'body'
})

module.exports = {
  entry: './code/src/index.js',
  output: {
    path: path.resolve(__dirname, 'build'),
    // below line only works for webpack 1.0
    // path: './dist', 
    filename: 'index_bundle.js'
  },
  devtool: '#source-map',
  module: {
    rules: [
      {
        test: /\.js$/, //Check for all js files
        loader: 'babel-loader',
        query: {
          presets: ["babel-preset-es2015"].map(require.resolve)
        },
        exclude: /node_modules/
      },
      {
        test: /\.jsx?$/,
        use: ['babel-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: "css-loader"
        })
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader'],
        })

      },
      {
        test: /bootstrap\/dist\/js\/umd\//,
        use: 'imports-loader?jQuery=jquery'
      }
    ]
  },
  resolve: {
    modules: [
      path.resolve('./code'),
      path.resolve('./node_modules')
    ]
  },
  plugins: [
    new ExtractTextPlugin("styles.css"),
    // new webpack.LoaderOptionsPlugin({
    //   options: {
    //     postcss: [autoprefixer]
    //   }
    // }),
    HtmlWebpackPluginConfig
    
  ]
}
