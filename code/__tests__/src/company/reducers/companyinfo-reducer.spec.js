import CompanyInfoReducer from 'src/company/reducers/companyinfo-reducer.js';

describe("Company Info Reducer functionality test", () => {

    it("should retun the initial state", () => {

        const initialState = {
            isLoading: false,
            hasError: false,
            companyName: "",
            companyMotto: "",
            companyEst: ""
        }

        expect(CompanyInfoReducer(undefined, {}))
            .toEqual(initialState);

    });

    it("when sumbit action is called should return the initial state", () => {

        const initialState = {
            isLoading: true,
            hasError: false,
            companyName: "",
            companyMotto: "",
            companyEst: ""
        }
        expect(CompanyInfoReducer(undefined,
            {
                type: 'SUBMIT_GETCOMPANY_INFO'
            })
        ).toEqual(initialState);
    });

    it("when finish action is called should retun the new state with correct json", () => {

        const expectedState = {
            isLoading: false,
            hasError: false,
            companyName: "westpac",
            companyMotto: "we work hard",
            companyEst: "1978"
        }

        let stubData = {
            companyName: "westpac",
            companyMotto: "we work hard",
            companyEst: "1978"
        };
        expect(CompanyInfoReducer(undefined, {
            type: 'FINISH_GETCOMPANY_INFO',
            data: stubData
        }))
            .toEqual(expectedState);
    });

    it("when companyinfo request fails, should update the reducer with hasError flag ", () => {

        const expectedState = {
            isLoading: false,
            hasError: true,
            companyName: "",
            companyMotto: "",
            companyEst: ""
        }

        let stubData = {
            companyName: "westpac",
            companyMotto: "we work hard",
            companyEst: "1978"
        };
        expect(CompanyInfoReducer(undefined, {
            type: 'ERROR_GETCOMPANY_INFO',
            data: stubData
        }))
            .toEqual(expectedState);

    });
});