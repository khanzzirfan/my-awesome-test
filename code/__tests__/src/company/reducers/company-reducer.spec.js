import CompanyDataReducer from 'src/company/reducers/company-reducer.js';

describe("Employees Reducer functionality test", () => {

    it("should retun the initial state", () => {

        const initialState = {
            isLoading: false,
            hasError: false,
            showModal: false,
            selectedEmployee: {},
            data: []
        }
        expect(CompanyDataReducer(undefined, {}))
            .toEqual(initialState);

    });

    it("when sumbit action is called should return the initial state", () => {

        const initialState = {
            isLoading: true,
            hasError: false,
            showModal: false,
            selectedEmployee: {},
            data: []
        }
        expect(CompanyDataReducer(undefined,
            {
                type: 'INITIALIZE_START',
                data: []
            })
        ).toEqual(initialState);
    });

    it("when finish action is called should retun the new state with correct json", () => {

        const expectedState = {
            isLoading: false,
            hasError: false,
            showModal: false,
            selectedEmployee: {},
            data: [{ "id": "1" }]
        }

        let stubData =
            [
                { "id": "1" }
            ];

        expect(CompanyDataReducer(undefined, {
            type: 'INITIALIZE_COMPLETE',
            data: stubData
        }))
            .toEqual(expectedState);
    });

    it("when Toggle modal action is called with current state of data, it should return selectedObject and show modal set to true", () => {

        const currentState = {
            isLoading: false,
            hasError: false,
            showModal: false,
            selectedEmployee: {},
            data: [{ "id": "1" }, { "id": "2" }]
        }

        const expectedState = {
            isLoading: false,
            hasError: false,
            showModal: true,
            selectedEmployee: { "id": "1" },
            data: [{ "id": "1" }, { "id": "2" }]
        }

        expect(CompanyDataReducer(currentState, {
            type: 'TOGGLE_MODAL',
            id: "1"
        })).toEqual(expectedState);
    });

    it("when get employee info api return error, should update company-reducer with hasError flag", () => {

        const expectedState = {
            isLoading: false,
            hasError: true,
            showModal: false,
            selectedEmployee: {},
            data: []
        }
        const currentState = {
            isLoading: false,
            hasError: false,
            showModal: true,
            selectedEmployee: { "id": "1" },
            data: [{ "id": "1" }, { "id": "2" }]
        }

        expect(CompanyDataReducer(currentState, {
            type: 'ERROR_GETEMPLOYEE_INFO',
        })).toEqual(expectedState);

    });


    it("when Toggle modal action is called with current state of show modal is true, it should return selectedObject none and show modal set to false", () => {

        const expectedState = {
            isLoading: false,
            hasError: false,
            showModal: false,
            selectedEmployee: {},
            data: [{ "id": "1" }, { "id": "2" }]
        }
        const currentState = {
            isLoading: false,
            hasError: false,
            showModal: true,
            selectedEmployee: { "id": "1" },
            data: [{ "id": "1" }, { "id": "2" }]
        }

        expect(CompanyDataReducer(currentState, {
            type: 'TOGGLE_MODAL',
            id: "1"
        })).toEqual(expectedState);

    });

});
