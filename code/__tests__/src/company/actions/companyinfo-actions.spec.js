import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import when from "when";
import * as Api from 'src/services/employee-api';
import * as CompanyInfoActions from 'src/company/actions/companyinfo-actions.js';;

describe("src/company/actions/company-info-action", () => {
    
    it("when getCompanyInfoData action is called, api is should return a valid json data", () => {

        let stubData = {
            companyInfo: { "companyName": "westpack" }
        };

        const expectedAction = [
            {
                type: 'SUBMIT_GETCOMPANY_INFO',
            },
            {
                type: 'FINISH_GETCOMPANY_INFO',
                data: stubData
            }
        ];

        //mock dummy promise api call
        Api.getCompanyInfo = jest.fn(() => when(stubData));

        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const store = mockStore({});

        return store.dispatch(CompanyInfoActions.getCompanyInfoData()).then(() => {
            // Test if your store dispatched the expected actions
            const actions = store.getActions();
            expect(actions.length).toBe(2);
            expect(actions).toEqual(expectedAction);
        });

    });


    it("when getCompanyInfoData action is called when API is unavailable, api is should throw an error", () => {

        let error = new Error("Error in getting data");

        //mock dummy promise api call
        Api.getCompanyInfo = jest.fn(() => when.reject(error));

        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const store = mockStore({});
        const expectedAction = [
            {
                type: 'SUBMIT_GETCOMPANY_INFO',
            },
            {
                type: 'ERROR_GETCOMPANY_INFO'
            }
        ];

        return store.dispatch(CompanyInfoActions.getCompanyInfoData()).then(() => {

        }).catch(err => {
            const actions = store.getActions();
            expect(actions.length).toBe(2);
            expect(actions).toEqual(expectedAction);
        });

    });

});