import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import when from "when";
import * as Api from 'src/services/employee-api';
import * as EmployeeActions from 'src/company/actions/employee-action.js';;

describe("src/company/actions/employee-action", () => {

    it("when employee toggle action is called, data is should return id", () => {

        let stubDataId = "2"
        const expectedAction = {
            type: 'TOGGLE_MODAL',
            id: stubDataId
        };

        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const store = mockStore({});

        store.dispatch(EmployeeActions.ToggleModal(stubDataId));
        // Test if your store dispatched the expected actions
        const actions = store.getActions()
        expect(actions).toEqual([expectedAction]);
    });


    it("when get employee action is called, data is should return valid jsonObject", () => {

        let stubData = {
            companyInfo: { "companyName": "westpack" }
        };

        const expectedAction = [
            {
                type: 'INITIALIZE_START',
                data: []
            },
            {
                type: 'INITIALIZE_COMPLETE',
                data: stubData
            }
        ];

        //mock dummy promise api call
        Api.getEmployees = jest.fn(() => when(stubData));

        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const store = mockStore({});

        return store.dispatch(EmployeeActions.getEmployeesData()).then(() => {
            // Test if your store dispatched the expected actions
            const actions = store.getActions()
            expect(actions).toEqual(expectedAction);
        });

    });

    it("when api is unavailable and  get data action is called it should throw an error", () => {

        const expectedAction = [
            {
                type: 'INITIALIZE_START',
                data: []
            },
            {
                type: 'ERROR_GETEMPLOYEE_INFO'

            }
        ];

        let error = new Error("Error in getting data");

        //mock dummy promise api call
        Api.getEmployees = jest.fn(() => when.reject(error));

        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        const store = mockStore({});

        return store.dispatch(EmployeeActions.getEmployeesData()).catch(err => {
            // Test if your store dispatched the expected actions
            const actions = store.getActions()
            expect(actions).toEqual(expectedAction);
        });

    });


});