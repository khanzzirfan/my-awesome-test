import * as React from "react";
import { mount, shallow } from "enzyme";
import EmployeeCardComponent from 'src/company/ui/components/employee-card-component';

describe("Employee Card Component test", () => {

    let mockFunc = jest.fn();
    let stubDataProps = {
        employee: {
            "id": "13856fb2-471f-48a0-bbc7-1e1cf1caf415",
            "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/rez___a/128.jpg",
            "firstName": "Violet",
            "lastName": "Langworth",
            "jobTitle": "International Group Assistant",
            "age": 40,
            "bio": "Tempora corporis ipsam modi iusto voluptatem.",
            "dateJoined": "2016-09-05T08:11:08.368Z"
        },
        onClick: mockFunc
    };
    let employeeCardComponentElement = null;

    beforeEach(() => {
        employeeCardComponentElement = shallow(<EmployeeCardComponent {...stubDataProps} />);
        expect(employeeCardComponentElement).toMatchSnapshot();
        expect(employeeCardComponentElement).toBeDefined();
    })

    it("the employee card component should render correctly with a given stub data", () => {
        let domElement;
        domElement = employeeCardComponentElement.find('#employee-card');
        expect(domElement).toBeDefined();

        domElement = employeeCardComponentElement.find('#employee-avatar');
        expect(domElement).toBeDefined();
        expect(domElement.length).toBe(1);

        domElement = employeeCardComponentElement.find('#employee-card .card-title');
        expect(domElement).toBeDefined();
        expect(domElement.text()).toEqual("Violet Langworth");

        domElement = employeeCardComponentElement.find('#employee-card .block-with-text');
        expect(domElement).toBeDefined();
        expect(domElement.text()).toEqual("Tempora corporis ipsam modi iusto voluptatem.");
    })

    it("test the component with click event", ()=>{
        let domElement;
        domElement = employeeCardComponentElement.find('#employee-card');
        domElement.simulate('click');
        expect(employeeCardComponentElement).toMatchSnapshot();
        
    });

    it("test the component with click event with arguments ", ()=>{
        let domElement;
        domElement = employeeCardComponentElement.find('#employee-card');
        domElement.simulate('click', {target:{value: '13856fb2-471f-48a0-bbc7-1e1cf1caf415'}});
        expect(mockFunc).toBeCalledWith('13856fb2-471f-48a0-bbc7-1e1cf1caf415');
        
    });

})