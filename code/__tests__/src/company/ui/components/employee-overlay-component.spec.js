import * as React from "react";
import { mount, shallow } from "enzyme";
import EmployeeOverlay from 'src/company/ui/components/employee-overlay-component';
import { FormattedDate } from 'src/modules/date-utils';
import _ from 'lodash';

describe("Overlay Component test", () => {
    var mockFunc = jest.fn();
    let stubDataProps = {
        employee: {
            "id": "13856fb2-471f-48a0-bbc7-1e1cf1caf415",
            "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/rez___a/128.jpg",
            "firstName": "Violet",
            "lastName": "Langworth",
            "jobTitle": "International Group Assistant",
            "age": 40,
            "bio": "Tempora corporis ipsam modi iusto voluptatem.",
            "dateJoined": "2016-09-05T08:11:08.368Z"
        },
        onClose: mockFunc,
        show: false
    };

    let overlayComponent = null;

    beforeEach(() => {
        overlayComponent = shallow(<EmployeeOverlay {...stubDataProps} />);
        expect(overlayComponent).toMatchSnapshot();
        expect(overlayComponent).toBeDefined();
    })

    it("the overlay component should render correctly with a given stub data", () => {
        let domElement;
        domElement = overlayComponent.find('#overlay');
        expect(domElement).toBeDefined();
    })

    it("the overlay component when show is false , it should not render modal content", () => {
        //stub data is initialized and show is set to false;
        stubDataProps = {
            employee: {},
            onClose: mockFunc,
            show: false
        };

        //render employee overlay
        overlayComponent = shallow(<EmployeeOverlay {...stubDataProps} />);
        expect(overlayComponent).toMatchSnapshot();

        let domElement;
        domElement = overlayComponent.find('#overlay .modal-content');
        expect(domElement.length).toBe(0);
    });

    it("the overlay component when show is true , it should render modal content", () => {
        //stub data is initialized and show is set to false;
        stubDataProps = {
            employee: {
                "id": "13856fb2-471f-48a0-bbc7-1e1cf1caf415",
                "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/rez___a/128.jpg",
                "firstName": "Violet",
                "lastName": "Langworth",
                "jobTitle": "International Group Assistant",
                "age": 40,
                "bio": "Tempora corporis ipsam modi iusto voluptatem.",
                "dateJoined": "2016-09-05T08:11:08.368Z"
            },
            onClose: mockFunc,
            show: true
        };

        //render employee overlay
        overlayComponent = shallow(<EmployeeOverlay {...stubDataProps} />);
        expect(overlayComponent).toMatchSnapshot();

        let domElement;
        domElement = overlayComponent.find('#overlay .modal-content');
        expect(domElement.length).toBe(1);

        domElement = overlayComponent.find('#overlay #emp-title');
        expect(domElement.text()).toEqual("International Group Assistant");

        domElement = overlayComponent.find('#overlay #emp-age');
        expect(domElement.text()).toEqual("Age: 40");

        domElement = overlayComponent.find('#overlay #emp-date-joined');
        expect(domElement.text()).toEqual("DateJoined: 05/09/2016");

        domElement = overlayComponent.find('#overlay #emp-fullname');
        expect(domElement.text()).toEqual("Violet Langworth");

        domElement = overlayComponent.find('#overlay #emp-biodata');
        expect(domElement.text()).toEqual("Tempora corporis ipsam modi iusto voluptatem.");

        domElement = overlayComponent.find('#overlay');
        let IMAGE_PATH = "https://s3.amazonaws.com/uifaces/faces/twitter/rez___a/128.jpg";

        expect(domElement.find('img').prop('src')).toEqual(IMAGE_PATH);

        expect(domElement.find('img').filterWhere((item) => {
            return item.prop('src') === IMAGE_PATH;
          })).toBeDefined();

    });

    it("the overlay component simulate the click event", () => {
        //stub data is initialized and show is set to false;
        stubDataProps = {
            employee: {
                "id": "13856fb2-471f-48a0-bbc7-1e1cf1caf415",
                "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/rez___a/128.jpg",
                "firstName": "Violet",
                "lastName": "Langworth",
                "jobTitle": "International Group Assistant",
                "age": 40,
                "bio": "Tempora corporis ipsam modi iusto voluptatem.",
                "dateJoined": "2016-09-05T08:11:08.368Z"
            },
            onClose: mockFunc,
            show: true
        };


        //render employee overlay
        overlayComponent = shallow(<EmployeeOverlay {...stubDataProps} />);
        expect(overlayComponent).toMatchSnapshot();

        let domElement;
        domElement = overlayComponent.find('#overlay');
        domElement.simulate('click', {
            targe: {
                value: ''
            }
        });
        expect(overlayComponent).toMatchSnapshot();
        expect(mockFunc).toHaveBeenCalled();
        expect(mockFunc).toBeCalledWith(0);
        
    });


});