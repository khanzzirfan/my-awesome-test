import { shallow, mount, render } from 'enzyme';
import React from 'react';
import CompanyContainer from 'src/company/ui/containers/company-container';
import configureStore from 'redux-mock-store';
const mockStore = configureStore();
import EmployeeOverlay from 'src/company/ui/components/employee-overlay-component';
import EmployeeCardComponent from 'src/company/ui/components/employee-card-component';

describe('Company Container jest snapshot', () => {
    var wrapper, store;
    var mockFunc = jest.fn();
    var stubContainerData = {
        companyData: {
            showModal: false,
            selectedEmployee: {},
            data: [
                {
                    id: '7ec2976d-adb1-40ee-9968-33301c91c359',
                    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/arpitnj/128.jpg',
                    firstName: 'David',
                    lastName: 'Collins',
                    jobTitle: 'District Branding Associate',
                    age: 41,
                    bio: 'Error illum reprehenderit ipsam hic error quasi sit odit qui. Dignissimos maxime numquam qui veritatis. Facilis quia enim.',
                    dateJoined: '2016-12-13T06:01:25.482Z'
                }
            ]
        },
        companyInfo: {
            companyName: 'Wolf, Kutch and Koss',
            companyMotto: 'killer optimize technologies',
            companyEst: '2013-06-14T07:40:47.699Z'
        }
    }

    beforeEach(() => {
        store = mockStore(stubContainerData);
        store.dispatch = jest.fn();
        wrapper = mount(<CompanyContainer store={store} />);
    })

    //snapshot testing
    it("container component should be defined", () => {
        expect(wrapper).toBeDefined();
        expect(wrapper).toMatchSnapshot();
    });

    it("container should render the component with correct html", () => {
        expect(wrapper.find('.card.card-outline-secondary').exists()).toBeTruthy();
        expect(wrapper.find('.card .card-block').exists()).toBeTruthy();

        expect(wrapper.find('.card .row').exists()).toBeTruthy();
        expect(wrapper.find('.card .text-sm-left.text-md-right').exists()).toBeTruthy();
        expect(wrapper.find('.row.mt20').exists()).toBeTruthy();
        expect(wrapper.find('.mt20.row.flexcontainer').exists()).toBeTruthy();
    });


    it("container should render the component with correct data", () => {
        expect(wrapper.find('#companyName').text()).toBe("Wolf, Kutch and Koss");
        expect(wrapper.find('#companyMotto').text()).toBe("killer optimize technologies");
        expect(wrapper.find('#companyEstdate').text()).toBe("since 14/06/2013 ");
    });

    it("container should render the nested flexbox component with correct data", () => {
        expect(wrapper.find(EmployeeOverlay).exists()).toBeTruthy();
        expect(wrapper.find(EmployeeCardComponent).exists()).toBeTruthy();
        expect(wrapper.find('FlexBoxElement').exists()).toBeTruthy();
    });

    it("when employee component clicked, should call the toggleMethod", () => {
        const connectedComponent = shallow(<CompanyContainer store={store} />).dive();
        const instance = connectedComponent.instance();
        jest.spyOn(instance, 'toggleModal');
        instance.toggleModal('d');

        expect(instance.toggleModal).toHaveBeenCalled();
    });
});