import { shallow, mount, render } from 'enzyme';
import React from 'react';
import ConnectedCompanyContainer, { CompanyContainer } from 'src/company/ui/containers/company-container';
import * as CompanyInfoActions from 'src/company/actions/companyinfo-actions.js';;
import * as Api from 'src/services/employee-api';
import * as EmployeeActions from 'src/company/actions/employee-action.js';
import when from "when";
import thunk from "redux-thunk";
import configureMockStore from 'redux-mock-store';

describe('Company Container', () => {
    var wrapper, store;

    beforeEach(() => {
        const middlewares = [thunk];
        const mockStore = configureMockStore(middlewares);
        store = mockStore(stubMockStore);
        wrapper = shallow(<ConnectedCompanyContainer store={store} />);

    });

    it('+++ render the connected(SMART) component', () => {
        expect(wrapper.length).toEqual(1)
    });

    it('+++ check Prop matches with initialState/mocked state', () => {
        expect(wrapper.props().employeeData).toEqual(stubMockStore.companyData);
        expect(wrapper.props().companyInfo).toEqual(stubMockStore.companyInfo);
    });


    it('maps state and dispatch to props', () => {
        expect(wrapper.props()).toEqual(expect.objectContaining({
            employeeData: expect.any(Object),
            companyInfo: expect.any(Object),
            employeeActions: expect.any(Object),
            companyInfoActions: expect.any(Object)
        }));
    });

    it('+++ check action on dispatching ', () => {
        let stubData = {
            companyInfo: { "companyName": "westpac" }
        };

        const expectedAction = [
            {
                type: 'INITIALIZE_START',
                data: []
            },
            {
                type: 'INITIALIZE_COMPLETE',
                data: stubData
            }
        ];

        //mock dummy promise api call
        Api.getEmployees = jest.fn(() => when(stubData));
        wrapper.props().employeeActions.getEmployeesData().then(() => {
            // Test if your store dispatched the expected actions
            const actions = store.getActions()
            expect(actions).toEqual([expectedAction]);
            done();
        });
    });

});


const stubMockStore = {
    companyData: {
        showModal: false,
        selectedEmployee: {},
        data: [
            {
                id: '7ec2976d-adb1-40ee-9968-33301c91c359',
                avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/arpitnj/128.jpg',
                firstName: 'David',
                lastName: 'Collins',
                jobTitle: 'District Branding Associate',
                age: 41,
                bio: 'Error illum reprehenderit ipsam hic error quasi sit odit qui. Dignissimos maxime numquam qui veritatis. Facilis quia enim.',
                dateJoined: '2016-12-13T06:01:25.482Z'
            },
            {
                id: 'dd413580-1cc3-4955-98fb-b74d6e09346a',
                avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/daykiine/128.jpg',
                firstName: 'Claire',
                lastName: 'Paterson',
                jobTitle: 'Direct Factors Representative',
                age: 20,
                bio: 'Est molestiae aliquam illo. Blanditiis autem dolores voluptatem vel iusto ut. Vel voluptas quae quibusdam ad. Voluptas quos perferendis occaecati tenetur quia iste quasi veritatis et. Totam velit a et.',
                dateJoined: '2015-10-03T09:56:10.938Z'
            },
            {
                id: '5609f782-b547-4354-bfe7-7993dadc6506',
                avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/rahmeen/128.jpg',
                firstName: 'Sam',
                lastName: 'Quigley',
                jobTitle: 'Internal Solutions Manager',
                age: 55,
                bio: 'Accusamus omnis laudantium sapiente expedita id beatae ad accusantium. Quis non est alias autem nulla possimus. Inventore praesentium quis odit quia. Perferendis nisi cupiditate qui natus alias.',
                dateJoined: '2014-01-22T15:33:47.360Z'
            }
        ]
    },
    companyInfo: {
        companyName: 'Wolf, Kutch and Koss',
        companyMotto: 'killer optimize technologies',
        companyEst: '2013-06-14T07:40:47.699Z'
    },
    routing: {
        location: {
            pathname: '/',
            search: '',
            hash: ''
        }
    }
}