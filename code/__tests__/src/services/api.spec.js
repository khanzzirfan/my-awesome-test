import * as Api from 'src/services/employee-api';
import * as CompanyInfoActions from 'src/company/actions/companyinfo-actions.js';;

describe("src/Services/employee-api", () => {

    describe("#GetEmployees when get employee api is called", () => {
        let actualResult;
        let fetchUrl = "http://localhost:3000/employees";

        beforeAll(function (done) {
            Api.httpGet = fetch.mockResponse(JSON.stringify(stubEmpoyeeInfo))
            Api.getEmployees().then(res => {
                actualResult = res;
                done();
            });
        })

        it("should call with correct URL", () => {
            expect(Api.httpGet.mock.calls[0][0]).toBe(fetchUrl);
            expect(Api.httpGet.mock.calls.length).toBe(1);
        });

        it("should return the correct data", ()=> {
            expect(actualResult.length).toEqual(2);
            expect(actualResult[0].id).toBe("844b410c-b9ff-4b0d-88d7-8632559e15a1");
            expect(actualResult[0].avatar).toBe("https://s3.amazonaws.com/uifaces/faces/twitter/isacosta/128.jpg");
            expect(actualResult[0].firstName).toBe("Amber");
            expect(actualResult[0].lastName).toBe("Quigley");
            expect(actualResult[0].jobTitle).toBe("Product Assurance Analyst");
            expect(actualResult[0].age).toBe(45);
            expect(actualResult[0].bio).toBe("Dolorem est quae autem.");
            expect(actualResult[0].dateJoined).toBe("2015-10-20T17:12:33.090Z");

        });

        
        it("should return error when api throws invaid exception", ()=> {
            var fetchUrl = "http://localhost:3000/companyInfo";
            let error = new Error("some error");
            Api.httpGet = fetch.mockReject(error);

            Api.getEmployees().catch(res => {
               expect(err).toEqual(error);
            });
        })
    });

   
    describe("#CompanyInfo when get company info api is called", () => {
        let actualResult;
        let fetchUrl = "http://localhost:3000/companyInfo";

        beforeAll(function (done) {
            fetch.resetMocks();
            Api.httpGet = fetch.mockResponse(JSON.stringify(stubCompanyInfo))
            Api.getCompanyInfo().then(res => {
                actualResult = res;
                done();
            });
        })

        it("should call with correct URL", () => {
            expect(Api.httpGet.mock.calls[0][0]).toBe(fetchUrl);
            expect(Api.httpGet.mock.calls.length).toBe(1);
        });

        it("should return the correct data", ()=> {
            expect(actualResult.companyName).toBe("Wolf, Kutch and Koss");
            expect(actualResult.companyMotto).toBe("killer optimize technologies");
            expect(actualResult.companyEst).toBe("2013-06-14T07:40:47.699Z");

        });


        it("should return error when api throws invaid exception", ()=> {
            let error = new Error("some error");
            Api.httpGet = fetch.mockReject(error);

            Api.getCompanyInfo().catch(res => {
               expect(err).toEqual(error);
            });
        })

    });

   
    describe("#httpGet when http get funtion is called", () => {
        let actualResult;
        let fetchUrl = "http://localhost:3000/companyInfo";
        let defaultGetOptions = {
            cache: 'no-cache',
            headers: {}
        }
        
        beforeAll(function (done) {
            fetch.resetMocks();
            window.fetch = fetch.mockResponse(JSON.stringify(stubCompanyInfo))
            Api.httpGet(fetchUrl).then(res => {
                actualResult = res;
                done();
            });
        });

        it("should call with correct URL", () => {
            expect(window.fetch.mock.calls.length).toBe(1);
            expect(window.fetch.mock.calls[0][0]).toBe(fetchUrl);
        });

    });


});



/**stub data for employeeinfo and companyinfo for testing api calls*/
const stubEmpoyeeInfo =[
    {
      "id": "844b410c-b9ff-4b0d-88d7-8632559e15a1",
      "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/isacosta/128.jpg",
      "firstName": "Amber",
      "lastName": "Quigley",
      "jobTitle": "Product Assurance Analyst",
      "age": 45,
      "bio": "Dolorem est quae autem.",
      "dateJoined": "2015-10-20T17:12:33.090Z"
    },
    {
      "id": "8a8ae5cf-c5dc-4a6a-acb7-ab21b8c79640",
      "avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/liminha/128.jpg",
      "firstName": "Lachlan",
      "lastName": "Dickinson",
      "jobTitle": "Internal Group Representative",
      "age": 20,
      "bio": "Sunt non nam dolor molestiae. Nulla officia eius commodi voluptatibus et. Illum molestiae nesciunt explicabo corporis quo animi placeat rerum expedita. Sed nesciunt et error ea ex earum omnis. Voluptatem maxime nemo ea quis libero rerum sit est alias. Qui qui fuga id aliquid ullam.",
      "dateJoined": "2010-06-04T18:07:23.025Z"
    },
]

const stubCompanyInfo = {
    "companyName": "Wolf, Kutch and Koss",
    "companyMotto": "killer optimize technologies",
    "companyEst": "2013-06-14T07:40:47.699Z"
  };