
/** dummy mock api call to fetch data*/
export function getEmployees() {
    return httpGet('http://localhost:3000/employees')
        .then(function (response) {
            return response;
        }).catch(error => {
            throw error;
        });
}

export function getCompanyInfo() {
    return httpGet('http://localhost:3000/companyInfo')
        .then(function (response) {
            return response;
        }).catch(error => {
            throw error;
        });
}


export function httpGet(url, options=defaultGetOptions){
   return window
    .fetch(url, options)
    .then(function(response) {
        return response.json();
      });
}

const defaultGetOptions = {
    cache: 'no-cache',
    headers: {}
}

