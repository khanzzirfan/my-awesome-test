import * as Api from 'src/services/employee-api';
import {
    INITIALIZE_START,
    INITIALIZE_COMPLETE,
    ERROR_GETEMPLOYEE_INFO,
    TOGGLE_MODAL
} from './types';


function SubmitInitializeEmployees() {
    return dispatch => {
        dispatch({
            type: INITIALIZE_START,
            data: []
        });
    }
}

function FinishInitializeEmployees(employeeData) {
    return dispatch => {
        dispatch({
            type: INITIALIZE_COMPLETE,
            data: employeeData
        });
    }
}

function OnError() {
    return dispatch => {
        dispatch({
            type: ERROR_GETEMPLOYEE_INFO,
        });
    }
}

export function ToggleModal(empId) {
    return dispatch => {
        dispatch({
            type: TOGGLE_MODAL,
            id: empId
        });
    }
}

export function getEmployeesData(){
    return dispatch => {
        dispatch(SubmitInitializeEmployees());
        return Api.getEmployees().then(response => {
            dispatch(FinishInitializeEmployees(response));
         })
         .catch(error => {
             dispatch(OnError());
             throw error;
         });
    }
}