import * as api from 'src/services/employee-api';
import {
    SUBMIT_GETCOMPANY_INFO,
    FINISH_GETCOMPANY_INFO,
    ERROR_GETCOMPANY_INFO
} from './types';


function Submit_GetCompanyInfo() {
    return dispatch => {
        dispatch({
            type: SUBMIT_GETCOMPANY_INFO
        });
    }
}


function Finish_GetCompanyInfo(companyData) {
    return dispatch => {
        dispatch({
            type: FINISH_GETCOMPANY_INFO,
            data: companyData
        });
    }
}

function OnError() {
    return dispatch => {
        dispatch({
            type: ERROR_GETCOMPANY_INFO,
        });
    }
}

export function getCompanyInfoData() {
    return dispatch => {
        dispatch(Submit_GetCompanyInfo());
        return api.getCompanyInfo().then(response => {
            dispatch(Finish_GetCompanyInfo(response));
        })
            .catch(error => {
                dispatch(OnError());
                throw error;
            });
    }
}
