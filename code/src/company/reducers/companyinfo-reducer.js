import {
    SUBMIT_GETCOMPANY_INFO,
    FINISH_GETCOMPANY_INFO,
    ERROR_GETCOMPANY_INFO
} from '../actions/types';

const initialState = {
    isLoading: false,
    hasError: false,
    companyName: "",
    companyMotto: "",
    companyEst: ""
}

const CompanyInfoReducer = (state = initialState, action) => {
    switch (action.type) {
        case SUBMIT_GETCOMPANY_INFO:
            return {
                isLoading: true,
                hasError: false,
                companyName: "",
                companyMotto: "",
                companyEst: ""
            }
        case FINISH_GETCOMPANY_INFO:
            return {
                ...state,
                isLoading: false,
                hasError: false,
                companyName: action.data.companyName,
                companyMotto: action.data.companyMotto,
                companyEst: action.data.companyEst
            }
        case ERROR_GETCOMPANY_INFO:
            return {
                ...state,
                isLoading: false,
                hasError: true,
                companyName: "",
                companyMotto: "",
                companyEst: ""
            }
        default:
            return state
    }
}

export default CompanyInfoReducer
