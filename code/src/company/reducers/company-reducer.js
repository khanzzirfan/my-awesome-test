import {
    INITIALIZE_START,
    INITIALIZE_COMPLETE,
    ERROR_GETEMPLOYEE_INFO,
    TOGGLE_MODAL
} from '../actions/types';


const initialState = {
    isLoading: false,
    hasError: false,
    showModal: false,
    selectedEmployee: {},
    data: []
}

const CompanyDataReducer = (state = initialState, action) => {
    switch (action.type) {
        case INITIALIZE_START:
            return {
                isLoading: true,
                hasError: false,
                showModal: false,
                selectedEmployee: {},
                data: []
            }
        case INITIALIZE_COMPLETE:
            return {
                ...state,
                isLoading: false,
                hasError: false,
                showModal: false,
                selectedEmployee: {},
                data: action.data
            }
        case TOGGLE_MODAL:
            let showModalPopUp = state.showModal == false ? true : false;
            let selectedEmp = {}
            if (showModalPopUp) {
                selectedEmp = state.data
                    .find(emp => emp.id === action.id);
            }
            return {
                ...state,
                showModal: showModalPopUp,
                selectedEmployee: selectedEmp,
            }
        case ERROR_GETEMPLOYEE_INFO:
            return {
                ...state,
                isLoading: false,
                hasError: true,
                showModal: false,
                selectedEmployee: {},
                data: []
            }
        default:
            return state
    }
}

export default CompanyDataReducer
