import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import s from '../company.scss';
import EmployeeCardComponent from '../components/employee-card-component';
import * as  EmployeeActions from 'src/company/actions/employee-action';
import * as  CompanyInfoActions from 'src/company/actions/companyinfo-actions';

import _ from 'lodash';
import { FormattedDate } from 'src/modules/date-utils';
import EmployeeOverlay from 'src/company/ui/components/employee-overlay-component';

class CompanyContainer extends Component {

    constructor(props) {
        super(props);

    }

    toggleModal = (empId) => {
        this.props.employeeActions.ToggleModal(empId);
    }

    componentDidMount() {
        this.props.companyInfoActions.getCompanyInfoData();
        this.props.employeeActions.getEmployeesData();
    }

    render() {
        const { showModal, selectedEmployee} = this.props.employeeData;
        const companyInfo = this.props.companyInfo;
        var employeeList = this.props.employeeData.data; 
        var companyEstDate = FormattedDate(companyInfo.companyEst);
        
        return (
            <div>
                <div className="card card-outline-secondary">
                    <div className="card-block">
                        <h3 id="companyName">{companyInfo.companyName}</h3>
                        <div className="row">
                            <div className="col-md-8">
                                <span id="companyMotto">{companyInfo.companyMotto}</span>
                            </div>
                            <div className="col-md-4 text-sm-left text-md-right">
                                <span id="companyEstdate">since {companyEstDate} </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row mt20 ">
                    <div id="our-employee" className="col-md-offset-1 col-md-11">
                        Our Employees
                    </div>
                </div>
                <hr />
                <div className="mt20 row flexcontainer">
                    {!!employeeList && employeeList.map((emp, index) => {
                        return (
                            <FlexBoxElement key={`row${index}`}
                                emp={emp}
                                handleToggle={this.toggleModal}
                            />
                        )
                    })}
                </div>

                <EmployeeOverlay show={showModal}
                    employee={selectedEmployee}
                    onClose={this.toggleModal}/>
            </div>
        );
    }
}

CompanyContainer.propTypes = {
    employeeData: PropTypes.object,
    companyInfo: PropTypes.object
};

function mapStateToProps(state) {
    return {
        employeeData: state.companyData,
        companyInfo: state.companyInfo
    }
}

function mapDispatchToProps(dispatch) {
    return {
        employeeActions: bindActionCreators(EmployeeActions, dispatch),
        companyInfoActions: bindActionCreators(CompanyInfoActions, dispatch),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CompanyContainer);


/*Flex box element  */
const FlexBoxElement = ({ children, ...props }) => {
    const { emp, handleToggle } = { ...props };
    return (
        <div className="p-2 employee-card" key={emp.id}>
            <EmployeeCardComponent onClick={handleToggle}
                employee={emp}
            />
        </div>
    );
}
