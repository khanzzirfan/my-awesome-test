import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedDate } from 'src/modules/date-utils';
import _ from 'lodash';

class EmployeeOverlay extends Component {

    hideModal = () => {
        this.props.onClose(0 /* dummy id to make selection null*/);
    }

    render() {

        const { employee, show } = this.props;
        const emp = employee;

        var showOverlayStyle = {
            display: 'block'
        };

        var hideOverlayStyle = {
            display: 'none'
        };

        var styleOverride = !!(this.props.show) ? showOverlayStyle : hideOverlayStyle;
        return (
            <div id="overlay" className="overlay" style={styleOverride} onClick={this.hideModal}>
                {
                    !(_.isEmpty(emp)) && (<div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header  text-right">
                                <span className="text-right"> </span>
                                <button type="button" className="close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body add-border">
                                <div className="row">
                                    <div className="col-md-4 col-sm-4">
                                        <img className="" id="overlay-avatar"
                                            src={`${emp.avatar}`}
                                            alt="Card image cap"
                                            height="120"
                                            width="100" />
                                    </div>
                                </div>
                                <div className="row row-bordered">
                                    <div className="col-md-4 col-sm-4">
                                        <div>
                                            <div id="emp-title">{`${emp.jobTitle}`}</div>
                                            <div id="emp-age">Age: {`${emp.age}`}</div>
                                            <div id="emp-date-joined">DateJoined: {FormattedDate(emp.dateJoined)}</div>
                                        </div>
                                    </div>
                                    <div className="col-md-8 col-sm-8">
                                        <h4 id="emp-fullname">{`${emp.firstName} ${emp.lastName}`}</h4>
                                        <hr className="hr-bottom-border"/>
                                        <div id="emp-biodata">
                                            <span>{`${emp.bio}`}</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>)
                }
            </div>
        );
    }
}

EmployeeOverlay.propTypes = {
    onClose: PropTypes.func.isRequired,
    show: PropTypes.bool,
    employee: PropTypes.object
};

export default EmployeeOverlay;