import React, { Component } from 'react';
import PropTypes from 'prop-types';

class EmployeeCardComponent extends Component {

    handleOnClick = () => {
        var empId = this.props.employee.id;
        this.props.onClick(empId);
    }

    render() {
        const emp = this.props.employee;
        var shortBio = emp.bio; //!!(emp.bio) ? emp.bio.substring(0, 35) : ''; data-toggle="modal" data-target="#myModal"
        return (
            <a href="javascript:void(0)" id="employee-card" onClick={this.handleOnClick}>
                <div className="employee-profile">
                    <img src={`${emp.avatar}`} alt="avatar" className="pull-left avatar" id="employee-avatar"/>
                    <div className="flex-item">
                        <div className="card">
                            <div className="card-block">
                                <h4 className="card-title">{`${emp.firstName} ${emp.lastName}`}</h4>
                                <p className="block-with-text ">{shortBio}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        );
    }
}

EmployeeCardComponent.propTypes = {
    onClick: PropTypes.func,
    employee: PropTypes.object
};

export default EmployeeCardComponent;

