import React from 'react';
import s from './App.scss';
import { Link } from 'react-router-dom';
import Routes from 'src/modules/routes';
import Bootstrap from 'bootstrap/dist/css/bootstrap.css';

const App = () =>
    <div className="container">
        { Routes }
    </div>;
export default App;