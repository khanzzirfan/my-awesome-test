import React from 'react';
import { Route, Switch } from 'react-router-dom';
import CompanyContainer from 'src/company/ui/containers/company-container';

export default (
    <Switch>
        <Route exact path="/" component={CompanyContainer} />
        <Route path="/about" component={About} />
    </Switch>
);



const About = () => {
    return (
        <div style={{ textAlign: 'center' }}>
            <h1>Westpac AU Test page</h1>
        </div>);

}