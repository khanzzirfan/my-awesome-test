/**
 * generally I use moment js to parse date objects
 * To limit to restrictions I created this function.
 */
export function FormattedDate(dateString) {
    var dateobj = new Date(dateString);
    function pad(n) { return n < 10 ? "0" + n : n; }
    var formattedDate = pad(dateobj.getDate()) + "/" + pad(dateobj.getMonth() + 1) + "/" + dateobj.getFullYear();
    return formattedDate;
}
