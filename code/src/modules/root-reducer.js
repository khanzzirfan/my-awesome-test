import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import CompanyDataReducer from 'src/company/reducers/company-reducer';
import CompanyInfoReducer from 'src/company/reducers/companyinfo-reducer';

const rootReducer = combineReducers({
 companyData: CompanyDataReducer,
 companyInfo: CompanyInfoReducer,
  routing: routerReducer
});

export default rootReducer;